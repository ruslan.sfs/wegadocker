# Wegadocker

Docker stack, based on WEGA project (https://github.com/WEGA-project).

## Setup

##### Setup config.

Copy env.example to env. Setup variables like you need.

Put to DATABASES var your databases naemes. Use ',' for separate. Dont use spaces!

##### Prepare and run

Recomended open and learn docker-compose.yml.
Grand permissions with:

```
chmod +x ./scripts/start.sh
```

And run:

```
./scripts/start.sh
```

##### Post setup

After start compose you have access (depends on how configured web service):
grafana gui on http://localhost:80/
wega gui on http://localhost:80/wega
wega api on http://localhost:8088/wega-api/wegabox.php

Also you have access to adminer on 9089 port and prometheus on 9090 port. You can also update
nginx config for provide it throw main access point. But usually it need only for development or debug.

###### Wega

Open wega, setup it. Make shure, you should have working view on dashboard, ex. temperature or humidity.
If you have emty data, you need do some thing for for work correctly.

First you must send data to db. Recomended for test send data manualy. You can send test data like:
http://localhost:8088/wega-api/wegabox.php?auth=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&db=dbname1&RootTemp=25&AirTemp=25&AirHum=50
Just check fields like auth and db in link and open it in your browser.
You must have success response.

Recomended init sql functions. For this go to home wega
http://localhost/wega

open any system by link in short name column, and just open params->calibration EC in top menu. It will be create sql functions.

Now you can go to params->database in top menu. You must see here you data which you sended in point 5.1. If all ok go next.

Setup your variables for database. Open params->field associations in top menu. Set up fild names. For our test you need set air temperature,
air humidity and root temp. Just set by name in description.

Now on home page you must see last sended data. If all ok, go next, else open adminer on localhost:8080, login and check real data in database. Also recomended check config table.

##### Now, when you have data, you can open grafana

Open http://localhost. Login. default login and pass admin admin. Recomended change password sure.

After login you need go to settings->data sources. Push add source, select prometheus and setup:
set Source link http://wega-prometheus:9090
set Scrape interval 60s (like in prometheus config)

Now go to explore panel select prometheus data source and metric (ex wega_temp). If you see data - all ok.

[![Dashboard](https://i.ibb.co/Pg27vCN/dashboard.png)](https://ibb.co/Pg27vCN)
Now you can setup dashboard as you need, but you can try my simple dashboard.
Just click in right side dashboards->browse, and click import button. Put in json field json data from /config/dashboard.json.
Select input data (in our case only one accesseble - Prometheus).

## License

For open source projects, say how it is licensed.

```

```
