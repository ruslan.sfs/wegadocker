<?php
include_once "db.php";
include "tstatus.php";
header("Content-Type: text/plain; version=0.0.4");

const DB_CONFIGS_DIR = './../wegagui/config';

$config_names = preg_grep('/^.*conf\.php$/', scandir(DB_CONFIGS_DIR));

function PrintInfoByDB($config_name)
{
    include DB_CONFIGS_DIR . "/" . $config_name;

    if (!$dbhost || !$login || !$password || !$my_db) {
        exit();
    } else {
        SetupDB($dbhost, $login, $password, $my_db, $tb);
        PrintStatus($my_db);
        CloseDB();
    }
}

foreach ($config_names as &$config_name) {
    PrintInfoByDB($config_name);
}

exit;
