<?php

include_once "db.php";

function dbval($p_name)
{
   $response = FetchObjectDB("select value from config where parameter = '$p_name' limit 1");

   return isset($response->value) ? $response->value : null;
}

function dbcomment($p_name)
{
   $response = FetchObjectDB("select comment from config where parameter = '$p_name' limit 1");

   return isset($response->comment) ? $response->comment : null;
}

function sensval($p_name)
{
   $response =  FetchObjectDB("select " . $p_name . " from sens order by dt desc  limit 1");

   return isset($response->$p_name) ? $response->$p_name : null;
}

function vallast($p_name)
{
   return FetchObjectDB("select " . $p_name . " as val,dt as dt from sens order by dt desc  limit 1");
}

function valprev($p_name, $sec)
{
   return FetchObjectDB("select " . $p_name . " as val,dt as dt from sens where UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(dt) > " . $sec . " order by dt desc  limit 1");
}

function valpred($p_name, $sec)
{
   return FetchObjectDB("select max(" . $p_name . ") as valmax,min(" . $p_name . ") as valmin from sens where UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(dt) > " . $sec . " limit 1");
}

function valdate($p_name, $date)
{
   return FetchObjectDB("select " . $p_name . " as value,dt as dt from sens where dt > '" . $date . "' order by dt limit 1");
}

function dbpsel($ns, $p, $comment)
{

   $tb = GetSensTable();
   $strSQL = "select * from $tb order by dt limit 1";

   $rs = QeryDB($strSQL);
   $numb = mysqli_num_rows($rs);

   mysqli_data_seek($rs, $numb - 1);
   $row = mysqli_fetch_row($rs);
   mysqli_data_seek($rs, 0);

   echo "<option selected>" . dbval($p, $ns) . "</option>";

   while ($property = mysqli_fetch_field($rs)) {
      echo "<option>";
      echo $property->name;
      echo "</option>";
   }
   echo "<option>null</option>";
}

function gplotgen($xsize, $ysize, $gimg, $wsdt, $wpdt, $csv, $handler, $text, $gnups, $img, $name, $nplot1, $nplot2, $nplot3, $nplot4, $nplot5, $dimens)
{
   global $LimitUP;
   global $LimitDOWN;

   $text = '
set terminal png size ' . $xsize . ',' . $ysize . '
set output "' . $gimg . '"
set datafile separator ";"
set xdata time
set format x "%d.%m\n%H:%M"
set timefmt "%Y-%m-%d %H:%M:%S"
set grid
set multiplot layout 1,1

set lmargin 10
set rmargin 10
set y2label
set xrange ["' . $wsdt . '" : "' . $wpdt . '"]
#set yrange [" ' . $LimitDOWN . ' ":" ' . $LimitUP . ' "]
#set yrange [' . $LimitDOWN . ':' . $LimitUP . ']
set title "' . $name . '"
set ylabel "' . $dimens . '"

fmax(x) = ' . $LimitUP . '
fmin(x) =' . $LimitDOWN . '

plot    \
	"' . $csv . '" using 1:2 w l title "' . $nplot1 . '", \
    "' . $csv . '" using 1:3 w l title "' . $nplot2 . '", \
    "' . $csv . '" using 1:4 w l title "' . $nplot3 . '", \
    "' . $csv . '" using 1:5 w l title "' . $nplot4 . '", \
    "' . $csv . '" using 1:6 w l title "' . $nplot5 . '" ';

   if ($LimitUP != "") {
      $text = $text . ',fmax(x) w l title ""';
   }
   if ($LimitDOWN != "") {
      $text = $text . ',fmin(x) w l title ""';
   }

   //echo $text;
   fwrite($handler, $text);
   fclose($handler);
   $err = shell_exec('cat ' . $gnups . '|gnuplot');
   echo "<br>";
   echo '<img src="' . $img . '" alt="альтернативный текст">';
}

function mixer_banks($pname)
{

   $mixerdb = "mixer";
   $sqlstr = "
select 
   dt as mdt, 
   v as v, 
   vbase as vbase,
   v*ro as ves    
from banks 
   where volume = '" . $pname . "' 
   order by dt desc 
   limit 1";

   $obj = FetchObjectDB($sqlstr);

   $mdt = $obj->mdt; // Дата установки раствора на миксер
   $v =  $obj->v; // Объем емкости в мл
   $vname = $obj->vbase; // Поле содержащее отмеренный вес
   $ves = $obj->ves; // Вес полной емксти


   $sqlstr = "select
    if(sum($vname) is not null,sum($vname),0) as sumves, # Суммарный расход из емкости в граммах
    $ves-if(sum($vname) is not null,sum($vname),0) as remain, # Остаток в емкости в граммах
    ($ves-if(sum($vname) is not null,sum($vname),0))/$ves*100 as procent # Остаток в процентах

from weght 
where dt > '" . $mdt . "' 
limit 1";


   return FetchObjectDB($sqlstr);
}

function showDate($date) // $date --> время в формате Unix time
{
   $stf      = 0;
   $cur_time = time();
   $diff     = $cur_time - $date;
   $seconds = array('секунда', 'секунды', 'секунд');
   $minutes = array('минута', 'минуты', 'минут');
   $hours   = array('час', 'часа', 'часов');
   $days    = array('день', 'дня', 'дней');
   $weeks   = array('неделя', 'недели', 'недель');
   $months  = array('месяц', 'месяца', 'месяцев');
   $years   = array('год', 'года', 'лет');
   $decades = array('десятилетие', 'десятилетия', 'десятилетий');

   $phrase = array($seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades);
   $length = array(1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600);

   for ($i = sizeof($length) - 1; ($i >= 0) && (($no = $diff / $length[$i]) <= 1); $i--) {;
   }
   if ($i < 0) {
      $i = 0;
   }
   $_time = $cur_time - ($diff % $length[$i]);
   $no    = floor($no);
   $value = sprintf("%d %s ", $no, getPhrase($no, $phrase[$i]));



   return $value;
}

function getPhrase($number, $titles)
{
   $cases = array(2, 0, 1, 1, 1, 2);

   return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
}
