<?php
include_once "func.php";

$p_AirTemp = dbval("AirTemp");

if ($p_AirTemp != 'null') {
    $p_AirTemp = "if(" . $p_AirTemp . " != 0," . $p_AirTemp . ",null)";
}

$p_AirHum = dbval("AirHum");
if ($p_AirHum != 'null') {
    $p_AirHum = "if(" . $p_AirHum . " != 0 and " . $p_AirHum . " < 100," . $p_AirHum . ",null)";
}

$p_AirPress = dbval("AirPress");
if ($p_AirPress != 'null' and $p_AirPress != '') {
    $p_AirPress = "if(" . $p_AirPress . " != 0," . $p_AirPress . ",null)";
}

$p_RootTemp = dbval("RootTemp");
if ($p_RootTemp != 'null') {
    $p_RootTemp = "if(" . $p_RootTemp . " != -127 and " . $p_RootTemp . " != 85," . $p_RootTemp . ",null)";
}

$p_ECtempRAW = dbval("ECtempRAW");

$p_pHraw = dbval("pHraw");
$p_IntTempRaw = dbval("IntTempRaw");
$p_VddRaw = dbval("VddRaw");
$p_LightRaw = dbval("LightRaw");
$p_Dst = dbval("Dst");
$p_CO2 = dbval("CO2");
$p_tVOC = dbval("tVOC");

// Блок ЕС
$P_A1 = dbval("A1");
$P_A2 = dbval("A2");

$R1 = floatval(dbval("EC_R1"));
$Rx1 = floatval(dbval("EC_Rx1"));
$Rx2 = floatval(dbval("EC_Rx2"));
$Dr = floatval(dbval("Dr"));

$P_R2p = "(-(-(" . $P_A1 . ")*" . $R1 . "-" . $P_A1 . "*" . $Rx2 . "+" . $Rx2 . "*" . $Dr . ")/(-" . $P_A1 . "+" . $Dr . "))";
$P_R2n = "(((-" . $P_A2 . "*" . $R1 . "-" . $P_A2 . "*" . $Rx1 . "+" . $R1 . "*" . $Dr . "+" . $Rx1 . "*" . $Dr . ")/" . $P_A2 . "))";
$P_R2 = "(" . $P_R2p . "+" . $P_R2n . ")/2";
$P_R2_polarity = $P_R2p . "-" . $P_R2n;

$R2p = round(sensval($P_R2p), 1);
$R2n = round(sensval($P_R2n), 1);
$R2 = ($R2p + $R2n) / 2;
$R2_polarity = round(sensval($P_R2_polarity), 2);
$p_ECtemp = "ftr(" . $p_ECtempRAW . ")";

$p_Lux = "fpr(" . $p_LightRaw . ")";
$p_pH = "pH(" . $p_pHraw . ")";

$p_EC = "if ( " . $p_ECtemp . " is null, null,EC(" . $P_A1 . "," . $P_A2 . "," . $p_ECtemp . "))";

$p_lev = "intpl(levmin(" . $p_Dst . "))";
$p_Pa = "Pa(" . $p_AirTemp . "," . $p_AirHum . ")";

$namesys = dbval("namesys");
$comment = dbcomment("namesys");
$LevelFull = dbval("LevelFull");
$LevelAdd = dbval("LevelAdd");
$La = dbval("La");
$Max_Level = $LevelFull - $La;
$ECPlan = dbval("ECPlan");
$sEC = dbval("sEC");
$rEC = dbval("rEC");
$Slk = ($sEC / $rEC);
$konc = dbval("konc");

$dt = sensval("dt");
$A1 = sensval($P_A1);
$A2 = sensval($P_A2);
$AirHum = sensval($p_AirHum);
$AirTemp = sensval($p_AirTemp);
$AirPress = sensval($p_AirPress);
$RootTemp = sensval($p_RootTemp);
$ECtempRAW = sensval($p_ECtempRAW);
$DstRAW = sensval($p_Dst);
$tempEC = sensval("ftR(" . $ECtempRAW . ")");
$WaterTemp = $tempEC;
$Lux = sensval("fpR(" . dbval("LightRAW") . ")");

$CO2 = sensval($p_CO2);
$tVOC = sensval($p_tVOC);

$ec = sensval("EC($P_A1,$P_A2," . $tempEC . ")");
$ph = sensval("ph(" . dbval("pHraw") . ")");
$lev = sensval("intpl(levmin(" . $p_Dst . "))");

$Pa = sensval("$p_Pa");

$L1 = $lev + $LevelAdd;
$L2 = $LevelFull - $lev - $La;
$ECn = (- ($ec * $L1 - $ECPlan * $L1 - $ECPlan * $L2) / $L2);
$Soiln = $ECn * $Slk * ($L2);
$p_soil = "($p_lev+" . $LevelAdd . ")*($p_EC*$Slk)";

// Пределы
$Max_OutDate = floatval(dbval("Ev_Max_Dt"));
$OutDate = strtotime("now") - strtotime($dt);
$Max_AirTemp = floatval(dbval("Ev_Max_AirTemp"));
$Min_AirTemp = floatval(dbval("Ev_Min_AirTemp"));
$Max_RootTemp = floatval(dbval("Ev_Max_RootTemp"));
$Min_RootTemp = floatval(dbval("Ev_Min_RootTemp"));
$Max_WaterTemp = floatval(dbval("Ev_Max_WaterTemp"));
$Min_WaterTemp = floatval(dbval("Ev_Min_WaterTemp"));
$Max_AirHum = floatval(dbval("Ev_Max_AirHum"));
$Min_AirHum = floatval(dbval("Ev_Min_AirHum"));
$Max_AirPress = floatval(dbval("Ev_Max_AirPress"));
$Min_AirPress = floatval(dbval("Ev_Min_AirPress"));
$Min_Level = floatval(dbval("Ev_Min_Level"));
$Crit_Level = floatval(dbval("Ev_Crit_Level"));
$Max_EC = floatval(dbval("Ev_Max_EC"));
$Min_EC = floatval(dbval("Ev_Min_EC"));
$Max_pH = floatval(dbval("Ev_Max_pH"));
$Min_pH = floatval(dbval("Ev_Min_pH"));
$Max_CO2 = floatval(dbval("Ev_Max_CO2"));
$Min_CO2 = floatval(dbval("Ev_Min_CO2"));

$mixerdb = dbval("mixerdb");

$gimg = $img;

//// Statistic
// pH changes
$timed = 60 * 60 * 72;
$val_last = vallast($p_pH, $timed)->val;
$dt_last = vallast($p_pH, $timed)->dt;
$val_prev = valprev($p_pH, $timed)->val;
$dt_prev = valprev($p_pH, $timed)->dt;
$sec = strtotime($dt_last) - strtotime($dt_prev);
$pHchanges = ($val_last - $val_prev) / $sec * 60 * 60 * 24;


// Level changes
$timed = 60 * 60 * 24;
$val_last = vallast($p_lev, $timed)->val;
$dt_last = vallast($p_lev, $timed)->dt;
$val_prev = valprev($p_lev, $timed)->val;
$dt_prev = valprev($p_lev, $timed)->dt;
$sec = strtotime($dt_last) - strtotime($dt_prev);
$LevChanges = ($val_last - $val_prev) / $sec * 60 * 60 * 24;

// EC Changes
$timed = 60 * 60 * 24;
$val_last = vallast($p_EC, $timed)->val;
$dt_last = vallast($p_EC, $timed)->dt;
$val_prev = valprev($p_EC, $timed)->val;
$dt_prev = valprev($p_EC, $timed)->dt;
$sec = strtotime($dt_last) - strtotime($dt_prev);
$ECchanges = ($val_last - $val_prev) / $sec * 60 * 60 * 24;
