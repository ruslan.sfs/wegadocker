<?php

function PrintMetric($name, $value, $type, $device, $sub, $help)
{
    if (!$name) {
        return false;
    }

    $fname = "wega_$name";

    if ($value != '-') {
        if ($help) {
            print("# HELP $fname $help.\n");
        }

        if ($type) {
            print("# TYPE $fname $type\n");
        }

        $resp = "$fname{device=\"$device\"";
        if ($sub) {
            $resp .= ",sub=\"$sub\"";
        }
        $resp .= "} $value\n";

        print($resp);
    }
}

function PrintMetricGauge($name, $value, $device, $sub, $help)
{
    return PrintMetric($name, $value, 'gauge', $device, $sub, $help);
}

function PrintStatus($device)
{
    include "sqvar.php";

    // $OutDate_Status = ($OutDate < $Max_OutDate) ? "1" : "0";
    // $AirHum_Status = '-';
    // $WaterTemp_Status = '-';
    // $AirTemp_Status = '-';
    // $pH_Status = '-';
    // $CO2_Status = '-';
    // $ec_Status = '-';
    // $lev_Status = '-';
    // $AirPress_Status = '-';
    // $RootTemp_Status = '-';

    $RelativeAirHumidity = '-'; //относительная влажность воздуха %
    $AbsoluteAirHumidity = '-'; //абсолютная влажность воздуха  г/м³
    $AirTempParsed = '-'; ///Температура воздуха: "°C"
    $AirPressParsed = '-'; //Давление воздуха:  мм. рт. ст.
    $RootTempParsed = '-'; //Температура в зоне корней:°C 
    $WaterTempParsed = '-'; //Температура раствора в баке °C
    $ecParsed = '-'; //Удельная электропроводность ЕС mS/cm
    $ECchangesParsed = '-'; // в сут
    $phParsed = '-'; //Водородный показатель pH
    $pHchangesParsed = '-'; //в сут
    $illumination = '-'; // Освещенность kLux
    $CO2Parsed = '-'; //Уровень CO2 ppm
    $aquaLevel = '-'; //Уровень раствора в баке литр
    $LevChangesParsed = '-'; //л/сут
    $totalBalanceAqua = '-'; //Общий остаток раствора в системе литр:

    if ($p_AirHum != 'null' and $p_AirHum != '') {
        // $AirHum_Status = ($AirHum < $Max_AirHum and $AirHum > $Min_AirHum) ? "success" : "problem";
        if ($AirHum) {
            $RelativeAirHumidity = round($AirHum, 1);
            $AbsoluteAirHumidity = round($Pa, 2);
        }
    }

    if ($p_AirTemp != 'null' and $p_AirTemp != '') {
        // $AirTemp_Status = ($AirTemp < $Max_AirTemp and $AirTemp > $Min_AirTemp) ? "success" : "problem";
        if ($AirTemp) {
            $AirTempParsed = round($AirTemp, 2);
        }
    }

    if ($p_AirPress != 'null' and $p_AirPress != '') {
        // $AirPress_Status = ($AirPress < $Max_AirPress and $AirPress > $Min_AirPress) ? "success" : "problem";
        if ($AirPress) {
            $AirPressParsed = round($AirPress, 2);
        }
    }

    if ($p_RootTemp != 'null' and $p_RootTemp != '') {
        // $RootTemp_Status = ($RootTemp < $Max_RootTemp and $RootTemp > $Min_RootTemp) ? "success" : "problem";

        if ($RootTemp) {
            $RootTempParsed = round($RootTemp, 2);
        }
    }

    if ($p_ECtempRAW != 'null' and $p_ECtempRAW != '') {
        // $WaterTemp_Status = ($WaterTemp < $Max_WaterTemp and $WaterTemp > $Min_WaterTemp) ? "success" : "problem";

        if ($WaterTemp) {
            $WaterTempParsed = round($tempEC, 2);
        }
    }

    if ($P_A1 != 'null' and $P_A2 != 'null' and $P_A1 != '' and $P_A2 != '') {
        // $ec_Status = ($ec < $Max_EC and $ec > $Min_EC) ? "success" : "problem";

        if ($ec) {
            $ecParsed = round($ec, 3);
            $ECchangesParsed = round($ECchanges, 3);
        }
    }

    if ($p_pHraw != 'null' and $p_pHraw != '') {
        // $pH_Status = ($ph < $Max_pH and $ph > $Min_pH) ? "success" : "problem";

        if ($ph) {
            $phParsed = round($ph, 3);
            $pHchangesParsed = round($pHchanges, 2);
        }
    }

    if ($p_LightRaw != 'null' and $p_LightRaw != '') {
        if ($Lux) {
            $illumination = round($Lux, 1);
        }
    }

    if ($p_CO2 != 'null' and $p_CO2 != '') {
        // $CO2_Status = ($CO2 < $Max_CO2 and $CO2 > $Min_CO2) ? "success" : "problem";

        if ($CO2) {
            $CO2Parsed = round($CO2, 3);
        }
    }

    if ($p_Dst != 'null' and $p_Dst != '') {
        // $lev_Status =  ($lev < $Max_Level and $lev > $Min_Level) ? "success" : "problem";
        if ($lev < $Crit_Level) {
            $lev_Status = "fail";
        }

        if ($lev) {
            $aquaLevel = round($lev, 1);
            $LevChangesParsed = round($LevChanges, 2);
            $totalBalanceAqua = round($L1, 1);
        }
    }

    PrintMetricGauge("relative_air_humidity", $RelativeAirHumidity, $device,  false, "относительная влажность воздуха %");
    PrintMetricGauge("absolute_air_humidity", $AbsoluteAirHumidity, $device,  false, "абсолютная влажность воздуха г/м³");

    PrintMetricGauge("air_press", $AirPressParsed, $device,  false, "давление воздуха мм. рт. ст");
    PrintMetricGauge("ec", $ecParsed, $device,  false, "удельная электропроводность ЕС mS/cm");
    PrintMetricGauge("ph", $phParsed, $device,  false, "водородный показатель pH");
    PrintMetricGauge("illumination", $illumination, $device,  false, "освещенность kLux");
    PrintMetricGauge("co2", $CO2Parsed, $device,  false, "уровень CO2 ppm");

    PrintMetricGauge("temp", $AirTempParsed, $device,  'air', "температура воздуха °C");
    PrintMetricGauge("temp", $RootTempParsed, $device, 'root', "температура в зоне корней °C");
    PrintMetricGauge("temp",  $WaterTempParsed, $device, 'water', "температура раствора в баке °C");

    PrintMetricGauge("changes", $pHchangesParsed, $device,  "ph", "в сут");
    PrintMetricGauge("changes", $ECchangesParsed, $device,  "ec", "в сут");
    PrintMetricGauge("changes",  $LevChangesParsed, $device, "lev", "л/сут");

    PrintMetricGauge("aqua_level", $aquaLevel, $device,  'main', "уровень раствора в баке литр");
    PrintMetricGauge("aqua_level", $totalBalanceAqua, 'remain',  "литр", "общий остаток раствора в системе литр");
}
