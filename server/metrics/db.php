<?php

$DB;
$SENS_TABLE;

function SetSensTable($val)
{
    global $SENS_TABLE;

    $SENS_TABLE = $val;
}

function GetSensTable()
{
    global $SENS_TABLE;

    return $SENS_TABLE;
}

function CloseDB()
{
    global $DB;
    if ($DB) {
        mysqli_close($DB);
    }
}

function GetDB()
{
    global $DB;
    return $DB;
}

function SetupDB($dbhost, $login, $password, $name, $sensTable)
{
    global $DB;

    $DB = mysqli_connect("$dbhost", "$login", "$password", "$name");

    SetSensTable($sensTable);
}

function QeryDB($sql)
{
    return mysqli_query(GetDB(), $sql);
}

function FetchObjectDB($sql)
{
    $link = QeryDB($sql);


    return $link ? mysqli_fetch_object($link) : null;
}
