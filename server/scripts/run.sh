#!/bin/bash

echo 'configuring wega';

SERVER_IP=$(hostname -I | sed -e "s/\s$//g")

echo "Create configs..."
echo "Set wegabox token"
cp /var/WEGA/wega-api/wegabox.php.example /var/WEGA/wega-api/wegabox.php
sed -i "s/^\$auth\=\".*\"\;/\$auth=\"$WEGA_API_TOKEN\";/g" /var/WEGA/wega-api/wegabox.php

echo "<?php \$dbhost=\"$DB_HOST\"; \$login=\"$DB_LOGIN\"; \$password=\"$DB_PASS\"; ?>" > /var/WEGA/db.php


IFS=',' read -ra databases <<< "$WEGA_UI_DATABASES"
for i in "${databases[@]}"
do
    echo "Create config for $i"
    echo "<?php \$dbhost=\"$DB_HOST\"; \$login=\"$DB_LOGIN\"; " > "/var/WEGA/wegagui/config/$i.conf.php"
    echo "\$password=\"$DB_PASS\"; \$my_db=\"$i\"; \$tb=\"sens\";?>" >> "/var/WEGA/wegagui/config/$i.conf.php"
done

chown -R www-data:www-data /var/WEGA

echo "date.timezone = \"$TZ\"" > /usr/local/etc/php/conf.d/tz.ini
chmod 755 /usr/local/etc/php/conf.d/tz.ini
# chown -R 777 /var/WEGA

cp "$PHP_INI_DIR/php.ini-$WEGA_ENV" "$PHP_INI_DIR/php.ini"

echo "WEGA_SERVER_IP:   http://$SERVER_IP/wega"
echo "WEGA_ENV:         $WEGA_ENV"
echo "WEGA_UI_USERNAME: $WEGA_USER_NAME"
echo "WEGA_UI_DATABASES $WEGA_UI_DATABASES"
echo 'String wegaapi  = "http://'$SERVER_IP'/wega-api/wegabox.php";'
echo 'String wegaauth = "'$WEGA_API_TOKEN'";'
echo 'String wegadb   = "esp32wega";'

echo "Start apache"
apache2-foreground